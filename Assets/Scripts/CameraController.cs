using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject player;
    private PlayerController playerController;

    private float trailingHeight;
    private float trailingDistance;
    private bool hittingSomething;

    private void Awake()
    {
        playerController = player.GetComponent<PlayerController>();
    }

    // runs after all other updates are done
    void LateUpdate()
    {
        // compute the new position of the camera
        var cameraOffset = player.transform.position;
        cameraOffset.z -= trailingDistance;
        cameraOffset.y += trailingHeight;

        // update the camera 
        transform.position = cameraOffset;
        transform.LookAt(player.transform);
    }

    private void OnTriggerStay(Collider other)
    {
        hittingSomething = true;
        trailingHeight += playerController.speed / 50 * Time.deltaTime;
        Debug.Log($"trigger - camera hitting {other.gameObject.name}!");
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log($"trigger - camera exit hitting {other.gameObject.name}!");
        hittingSomething = false;
    }

    void OnLook(InputValue lookValue)
    {
        var mouseDelta = lookValue.Get<Vector2>();
        Debug.Log($"look - mouse delta: {mouseDelta}");

        trailingDistance = Mathf.Clamp(trailingDistance + mouseDelta.x * Time.deltaTime, 5, 12);
        if (!hittingSomething)
        {
            trailingHeight = Mathf.Clamp(trailingHeight + mouseDelta.y * Time.deltaTime, 1, 20);
        }
        Debug.Log($"look - trailing height: {trailingHeight} and distance: {trailingDistance}");
    }

    private void OnDrawGizmos()
    {
        // show the vertices of the near clip place of the main camera
        var camera = Camera.main;
        Vector3 topLeft = camera.ViewportToWorldPoint(new Vector3(0, 1, camera.nearClipPlane));
        Vector3 topRight = camera.ViewportToWorldPoint(new Vector3(1, 1, camera.nearClipPlane));
        Vector3 bottomLeft = camera.ViewportToWorldPoint(new Vector3(0, 0, camera.nearClipPlane));
        Vector3 bottomRight = camera.ViewportToWorldPoint(new Vector3(1, 0, camera.nearClipPlane));

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(topLeft, 0.1f);
        Gizmos.DrawSphere(topRight, 0.1f);
        Gizmos.DrawSphere(bottomLeft, 0.1f);
        Gizmos.DrawSphere(bottomRight, 0.1f);
    }
}
