using UnityEngine;

public class PickupAnimator : MonoBehaviour
{
    [SerializeField] private Vector3 rotationAmount;
    [SerializeField] private float amount;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rotationAmount.shake(amount) * Time.deltaTime);
    }
}
