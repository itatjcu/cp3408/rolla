using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{

    [field: SerializeField, Range(1f, 1000f)]
    public float speed { get; private set; }

    private new Rigidbody rigidbody;
    private Vector3 movement;

    // Start is called before the first frame update
    void Start()
    {
        // called on the first frame the script is activated on
        rigidbody = GetComponent<Rigidbody>();
        movement = Vector3.zero;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Vector3 line = transform.TransformDirection(Vector3.forward);
        line *= 2;
        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, line);
    }
#endif

    private void FixedUpdate()
    {
        // called before the physics system
        rigidbody.AddForce(movement * speed);
    }

    // called by InputSystem package
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        Debug.Log($"player - movementVector: {movementVector}");

        // note: x,z is how the ground plane is orientated
        movement.x = movementVector.x;
        movement.z = movementVector.y;
        //movement = movementVector; // try this for giggles :)
    }

    // rather than pickups as triggers, we check "every" collision the player has
    private void OnCollisionEnter(Collision collision)
    {
        GameObject gameObject = collision.gameObject;

        Debug.Log($"player - OnCollisionEnter: {gameObject}");

        switch (gameObject.tag)
        {
            case "pickup":
                gameObject.SetActive(false);
                break;
            case "elephant":
#if UNITY_EDITOR
                Vector3 impulse = collision.impulse.normalized;
                // project onto x,z plane
                Vector3 xzImpulse = Vector3.ProjectOnPlane(impulse, Vector3.down);

                Debug.Log($"player - impulse: {impulse} xzImpulse: {xzImpulse}");

                Debug.DrawRay(transform.position, xzImpulse *= 3, Color.red, 3);
#endif
                break;
        }
    }
}
