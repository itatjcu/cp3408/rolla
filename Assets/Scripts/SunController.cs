using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunController : MonoBehaviour
{
    private Vector3 euler;

    private void Awake()
    {
        euler = transform.rotation.eulerAngles;
    }

    private void LateUpdate()
    {
        // rotate the "sun" around it's y axis slowly
        // this causes it to shift it's apparent position in the scene!
        transform.rotation = Quaternion.Euler(euler);

        euler.y += 10 * Time.deltaTime;
    }
}
