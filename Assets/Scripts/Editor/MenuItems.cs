using UnityEditor;
using UnityEngine;

public class MenuItems
{

    [MenuItem("Tools/Reset Object &r")]
    static void ResetObject()
    {
        GameObject[] selection = Selection.gameObjects;
        if (selection.Length < 1) return;

        foreach (GameObject gameObject in selection)
        {
            InternalZeroPosition(gameObject);
            InternalZeroRotation(gameObject);
            InternalZeroScale(gameObject);
        }

        Undo.RegisterCompleteObjectUndo(selection, "Reset Object");
    }

    [MenuItem("Tools/Reset Object Position and Rotation &#r")]
    static void ResetObjectPositionRotation()
    {
        GameObject[] selection = Selection.gameObjects;
        if (selection.Length < 1) return;

        foreach (GameObject gameObject in selection)
        {
            InternalZeroPosition(gameObject);
            InternalZeroRotation(gameObject);
        }

        Undo.RegisterCompleteObjectUndo(selection, "Reset Object Position and Rotation");
    }

    #region helper methods
    private static void InternalZeroPosition(GameObject gameObject)
    {
        gameObject.transform.localPosition = Vector3.zero;
    }
    private static void InternalZeroRotation(GameObject gameObject)
    {
        gameObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }
    private static void InternalZeroScale(GameObject gameObject)
    {
        gameObject.transform.localScale = Vector3.one;
    }
    #endregion
}
