using UnityEngine;

public static class Utilities
{
    public static Vector3 shake(this Vector3 vector, float amount)
    {
        var offset = new Vector3
        {
            x = amount * Random.Range(-1.0f, 1.0f),
            y = amount * Random.Range(-1.0f, 1.0f),
            z = amount * Random.Range(-1.0f, 1.0f)
        };

        vector += offset;
        return vector;
    }
}
